vcf-splitter
====

Description:
------------
A small script to filter content of a .vcf file.

Use the `--value` flag to filter the content of fields.
Use the `--property` flag to filter entries containing the specified field.

```
# Filters the VCARD entries with an EMAIL field.
$ vcf-splitter.py --property EMAIL input.vcf

# Filters the VCARD entries containing STRING in any field.
$ vcf-splitter.py --value STRING input.vcf

# Filters the VCARD entries with a defined EMAIL field and containing STRING in
# any field.
$ vcf-splitter.py --value STRING --property EMAIL input.vcf
```

Installation:
-------------
Copy the python script somewhere in the `PATH` or use installation script `install.py`.
See `./install.py --help`.

Usage:
------
```
vcf_filter.py [-h] [-v STRING] [-p STRING] [input_files ...]

A small script for filtering VCARD entries out of .vcf files

positional arguments:
  input_files           list of input files in VCARD format (STDIN if no input
                        file provided)

options:
  -h, --help            show this help message and exit
  -v STRING, --value STRING
                        filter the entries containing STRING value
  -p STRING, --property STRING
                        filter the entries containing STRING property (e.g.
                        N/FN/TEL/EMAIL)
```
