#!/usr/bin/env python3

import argparse
import sys


def split_vcf(f, content_filter=None):
    """
    split_vcf: splits the content of a vcf file into a list of strings, each string containing a single VCARD entry.
    @f: an opened vcf file
    @content_filter: an optional string containing the required content
    """
    l = []
    content = ""
    for line in f:
        content += line
        if line.find("END:VCARD") == 0:
            if content_filter is None or any(
                map(lambda ln: ln.find(content_filter) == 0, content.splitlines())
            ):
                l.append(content)
            content = ""
    return l


def vcard_contains_value(vcard, value):
    """
    vcard_contains_value: returns true if a field in vcard contains a value

    @vcard: a str containing a single vcard entry
    @value: a str containing the value to look for
    """
    for line in vcard.split("\n"):
        try:
            if not ":" in line:
                # print(f"DBG: ':' not found in '{line}'", file=sys.stderr)
                continue
            elif value in line.split(":")[1]:
                return True
        except:
            raise Exception("Invalid line in VCARD entry: {}".format(line))
    return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="A small script for filtering VCARD entries out of .vcf files",
    )

    parser.add_argument(
        "input_files",
        help="list of input files in VCARD format (STDIN if no input file provided)",
        nargs="*",
        type=argparse.FileType("r", encoding="UTF-8"),
        default=[sys.stdin],
    )
    parser.add_argument(
        "-v",
        "--value",
        help=f"filter the entries containing %(metavar)s value",
        metavar="STRING",
    )
    parser.add_argument(
        "-p",
        "--property",
        help=f"filter the entries containing %(metavar)s property (e.g. N/FN/TEL/EMAIL)",
        metavar="STRING",
    )

    args = parser.parse_args()
    result = []
    for f in args.input_files:
        result += split_vcf(f, args.property)
        f.close()
    if args.value is not None:
        result = list(
            filter(lambda vcard: vcard_contains_value(vcard, args.value), result)
        )
    print("".join(set(result)))
